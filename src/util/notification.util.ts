import { MyContext, MyRequest } from '../notification/notification.type';

const DUMMY_REQUEST_ID = '00000000-0000-0000-0000-000000000000';

export function extractRequest(request: MyRequest): MyContext {
  /* istanbul ignore next */
  return {
    requestId:
      request.apiGateway?.event.requestContext.requestId ?? DUMMY_REQUEST_ID,
  };
}

export function buildRequest({
  isAuthorized,
  targetid,
  userid,
}: {
  isAuthorized: boolean;
  targetid?: string;
  userid?: string;
}): MyRequest {
  return {
    apiGateway: {
      event: {
        headers: { targetid, userid },
        requestContext: {
          authorizer: { isAuthorized: String(isAuthorized) },
          requestId: DUMMY_REQUEST_ID,
        },
      },
    },
  };
}
