import { Module, OnModuleInit } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { DynamooseModule } from 'nestjs-dynamoose';
import { NotificationModule } from './notification/notification.module';
import { log, logger } from './util/logger.util';

@Module({
  imports: [
    ConfigModule.forRoot(),
    DynamooseModule.forRoot({
      local: process.env.IS_DDB_LOCAL === 'true',
      aws: { region: process.env.REGION },
      model: {
        create: false,
        prefix: `${process.env.SERVICE}-${process.env.STAGE}-`,
      },
    }),
    NotificationModule,
  ],
})
export class AppModule implements OnModuleInit {
  onModuleInit() {
    const logLevel = process.env.LOG_LEVEL
      ? process.env.LOG_LEVEL
      : log.levels.DEBUG;
    log.setDefaultLevel(logLevel as log.LogLevelDesc);
    logger.info('app.module onModuleInit()');
  }
}
