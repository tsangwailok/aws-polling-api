export type MyRequest = {
  apiGateway?: {
    event: {
      headers: { targetid?: string; userid?: string };
      requestContext: {
        authorizer: { isAuthorized?: string };
        requestId: string;
      };
    };
  };
};

export type MyContext = {
  requestId: string;
};
