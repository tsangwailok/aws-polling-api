import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Request,
} from '@nestjs/common';
import { logger } from '../../util/logger.util';
import { extractRequest } from '../../util/notification.util';
import { CreateNotificationInput } from '../model/create-notification.input';
import { UpdateNotificationInput } from '../model/update-notification.input';
import { MyRequest } from '../notification.type';
import { NotificationService } from '../service/notification.service';

@Controller('notification')
export class NotificationController {
  constructor(private readonly notificationService: NotificationService) {}

  @Post()
  create(@Body() body: CreateNotificationInput, @Request() req: MyRequest) {
    const ctx = extractRequest(req);
    logger.info(ctx.requestId, 'create notification');
    return this.notificationService.create(body);
  }

  @Get(':id')
  findOne(@Param('id') id: string, @Request() req: MyRequest) {
    const ctx = extractRequest(req);
    logger.info(ctx.requestId, 'get notification by id:', id);
    return this.notificationService.findOne({ id });
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() body: UpdateNotificationInput) {
    return this.notificationService.update({ id }, body);
  }

  @Get()
  find(@Query() { userId, targetId }: { userId?: string; targetId?: string }) {
    if (userId && !targetId) {
      return this.notificationService.findByUserId(userId);
    }
    if (targetId && !userId) {
      return this.notificationService.findByTargetId(targetId);
    }
    throw new BadRequestException();
  }
}
